﻿using System;
using NUnit.Framework;
using ProductOfArrayItems;

namespace ProductOfArrayItemsTests
{
    [TestFixture]
    public class KataTests
    {
        [Test]
        public void Product_DataTest()
        {
            Assert.AreEqual(540, Kata.Product(new int[]{ 5, 4, 1, 3, 9 }));
            Assert.AreEqual(-672, Kata.Product(new int[]{ -2, 6, 7, 8 }));
            Assert.AreEqual(10, Kata.Product(new int[] { 10 }));
            Assert.AreEqual(0, Kata.Product(new int[] { 0, 2, 9, 7 }));
        }
        
        [Test]
        public void Product_ArgumentNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => {
                Kata.Product(null);
            });
        }
        
        [Test]
        public void Product_InvalidOperationTest()
        {
            Assert.Throws<InvalidOperationException>(() => {
                Kata.Product(new int[]{});
            });
        }
    }
}