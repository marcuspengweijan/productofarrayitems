﻿using System;
using System.Linq;

namespace ProductOfArrayItems
{
    public class Kata
    {
        public static int Product(int[] values)
        {
            return values.Aggregate((result, item) => result * item);
        }
    }
}